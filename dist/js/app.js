$(document).ready(function(){
let tempScrollTop, 
    currentScrollTop = $(window).scrollTop();

//-------------- Fix navigation__List ----------------------
//when refreshing the page while standing on the footer, 
//so that the footer animation is played
setTimeout(function(){
  footerAnimate()
  const navigationList = $('.js-navigation__item');
  if ($(window).scrollTop() > $('.home').height()) {
    for (let navigationItem of navigationList) {
      navigationItem.style.visibility = 'visible';
    }
  }
}, 1000);

//-------------- Mobile img ----------------------
window.addEventListener(`resize`, function(){
  if( $(window).width() < 1024 ) {
    $('.js-about__foto-me-img').attr('src', 'img/about/about__me--mobile.png');
  } else {
    $('.js-about__foto-me-img').attr('src', 'img/about/about__me.png');
  }
}, false);

//------------- Mouse move title Me ----------------------
  $('.home').on('mousemove', (e) => {
    const x = e.pageX / $(window).width();
    const y = e.pageY / $(window).height();
    if (document.body.clientWidth > 1023) {
      $('.home__me').css(
        'transform',
        'translate(' + x * 15 + 'px, ' + y * 15 + 'px)'
      );
    }
  })

//------------- Menu Burger -------------------------------
  function menuBurger(selector){
    let menu = $(selector);
    let button = menu.find('.menu-burger__button');
    let links = menu.find('.menu-burger__link');
    let overlay = menu.find('.menu-burger__overlay');
    button.on('click', (e) => {
      e.preventDefault();
      toggleMenu();
    });
    links.on('click', () => toggleMenu());
    overlay.on('click', () => toggleMenu());
    
    function toggleMenu() {
      menu.toggleClass('menu-burger--active');
      if (menu.hasClass('menu-burger--active')) {
        $('body').css('overflow', 'hidden');
      } else {
        $('body').css('overflow', 'visible');
      }
    }
  }
  menuBurger('.menu-burger');

//------------- Navigation on scroll-up ------------------
  $(window).scroll(function(){ 
    footerAnimate();
    currentScrollTop = $(window).scrollTop();
      if ((currentScrollTop > $('.home').height()) && ( tempScrollTop > currentScrollTop )) { 
        $('nav').addClass('fixed-header');
        if ( tempScrollTop > currentScrollTop ) {
          $('.js-navigation__list').addClass('show');
        } else {
          $('.js-navigation__list').removeClass('show');
        }
      } else {
        $('.js-navigation__list').removeClass('show');
        if(currentScrollTop < $('.home').height() / 2){
          $('nav').removeClass('fixed-header');
        }
      }
        tempScrollTop = currentScrollTop;
  });

  new WOW().init();
});

//------------- Slow Scroll -------------------------------
function slowScroll(id) {
  let offset = 0;
  $('html, body').animate({
    scrollTop: $(id).offset().top - offset}, 500);
  return false;
}

//------------- Footer Animate -----------------------------
function footerAnimate(){    //fixed footer did not allow animation to work correctly
  if ((($('.home').height() - document.querySelector('.js-main-content__marker-end').getBoundingClientRect().top) > 0)
    && !($(".js-footer__title").hasClass("animated"))) {
      $('.js-footer__title').addClass('wow animated rubberBand').css('animation-delay', '1s');
      $('.js-footer__text').addClass('wow animated rubberBand').css('animation-delay', '1.5s');
      $('.js-contact__gitlab').addClass('wow animated fadeInBottomLeft').css('animation-delay', '2s');
      $('.js-contact__github').addClass('wow animated fadeInBottomRight').css('animation-delay', '2s');
      const socialList = $('.js-contact__social-item');
      let a = 3;
      for (let socialItem of socialList) {
        socialItem.classList.add('wow', 'animated', 'bounceInDown');
        socialItem.style['animation-delay'] = a + 's';
        a += 0.1;
      }
      setTimeout(function(){
        $('.js-contact__gitlab').removeClass('wow animated fadeInBottomLeft').addClass('hover-git').css('animation-delay', '0s');
        $('.js-contact__github').removeClass('wow animated fadeInBottomRight').addClass('hover-git').css('animation-delay', '0s');
        for (let socialItem of socialList) {
          socialItem.classList.remove('wow', 'animated', 'bounceInDown');
          socialItem.classList.add('hover-social');
          socialItem.style['animation-delay'] = 0;
        }
      }, 5000);
  }
}